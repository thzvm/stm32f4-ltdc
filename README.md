# LCD-TFT display controller (LTDC) on STM32 MCUs #

### SOFTWARE PROJECT ###

* Project Name: 						STM32F4 LTDC
* MCU:									STM32F429ZITx
* Toolchain / IDE: 						MDK-ARM V5
* STM32CubeMX:						 	4.20.1
* Firmware Package Name and Version:	STM32Cube FW_F4 V1.16.0
* Compiler Optimizations: 				Balanced Size/Speed

### STRUCTURE ###
![KEIL.JPG](https://bitbucket.org/repo/z88e9GE/images/4076753188-KEIL.JPG)